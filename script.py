import pandas as pd

#need to change URL to new one !!!!!!!!!
#df = pd.read_csv('https://drive.google.com/u/0/uc?id=18P_e_WGXIJ-RrVK6bDz8FGh6jnwpsl6d&export=download') 
df = pd.read_csv('https://drive.google.com/u/0/uc?id=1S9xC57vIGypktix6qBwNBqYQlJ8NkTHk&export=download  ')
print("Welcome to our hotel recommendation.")

colNames = []
bindTogether = {}
for col in df.columns:
    colNames.append(col)

#print(df)
print("Colnames: ", colNames)

i=0
notCorrect = 0

print("Chose important factors for your hotel recommendation.")
for col in colNames:
    print(i, " ", colNames[i])
    bindTogether[str(i)] = colNames[i]
    i = i + 1

chosen = []
valuesChosen = input("Simply type numbers like \'1 3 5 11\': ") 
chosen = valuesChosen.strip().split(' ')

chosenFilters = {}
counter = 0
print(chosen)

#Works til here

for number in chosen:
    counter = counter + 1
    attrName = bindTogether[str(number)]
    uniqueList = df[attrName].unique()
    print("Possible values for", attrName, ":")
    for i in range(0,len(uniqueList)):
        print(uniqueList[i])
    print("Choose from options or write 0 if nothing fits you.")
    value = input()

    while notCorrect == 0:
        #if ((value in uniqueList) or (value == 0)):
        if (int(value) in uniqueList) or (value == 0) :
            break
        else:
            print("Wrong input. Write something else")
            value = input()

    if value != 0:
        chosenFilters[attrName] = value
        #print("attr name: ", attrName, "value: ", value)
        df = df.loc[ df[attrName] == int(chosenFilters[attrName]) ]
        #df = df.loc[ df["atr1"] == 1 ]
        #print(df)
    
    numberOfHotels = len(df.index)
    print("So far we can recommend ", numberOfHotels, "hotels like these: ")
    #CHANGE NEXTLINE for actual cols
    #df['atr1','atr2']
    print(df.head())

    if counter < len(chosen):
        goFurther = input("Do you wish to continue filtering? yes/no  :")

        if goFurther == 'no' or goFurther == 'No':
            break
        
print("Thank you for using our recommendation system!")